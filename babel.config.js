/**
 * ----------------------------------------------------------------
 * @Author      : Mathieu Boutin
 *
 * @File        : babel.config.js
 * @Created_at  : 26/03/2022
 * @Update_at   : 26/03/2022
 * ----------------------------------------------------------------
 */

module.exports = {
    presets: [
        '@vue/cli-plugin-babel/preset'
    ]
};
