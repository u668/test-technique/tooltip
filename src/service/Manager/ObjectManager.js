/**
 * ----------------------------------------------------------------
 * @Author     : Mathieu Boutin
 *
 * @File       : ObjectManager.js
 * @Created_at : 26/03/2022
 * @Update_at  : 26/03/2022
 * ----------------------------------------------------------------
 */


import TypeManager from "./TypeManager";
export default class ObjectManager {
    // --------------------------------
    // Check Instance

    /**
     * Check if object is an instance of {objectClass}
     *
     * @param object
     * @param objectClass
     *
     * @return {boolean}
     */
    static is(object, objectClass) {
        // ----------------
        // Vars

        let is = false;

        // ----------------
        // Process

        // Instance
        try {
            return object instanceof objectClass;
        } catch (e) {
            is = false;
        }

        // Constructor
        try {
            return object instanceof objectClass.constructor;
        } catch (e) {
            is = false;
        }

        return is;
    }

    // --------------------------------
    // Has

    /**
     * Check if object have key.
     *
     * @param object
     * @param key
     * @param nullable
     *
     * @return {boolean}
     */
    static hasKey(object, key, nullable = false) {
        // ----------------
        // Process

        return TypeManager.isObject(object)
            && Object.prototype.hasOwnProperty.call(object, key)
            && (nullable ? true : !TypeManager.isNull(object[key]));
    }

    /**
     * Check if object have method.
     *
     * @param object
     * @param method
     *
     * @return {boolean}
     */
    static hasMethod(object, method) {
        // ----------------
        // Process

        return TypeManager.isObject(object)
            && TypeManager.isFunction(object[method]);
    }


    // --------------------------------
    // Getters

    /**
     * Get key in object.
     *
     * @param object
     * @param key
     * @param nullable
     * @param defaultValue
     *
     * @return {*}
     */
    static get(object, key, nullable = false, defaultValue = null) {
        // ----------------
        // Check

        if (!this.hasKey(object, key, nullable)) return defaultValue;

        // ----------------
        // Process

        return object[key];
    }

    /**
     * Get object keys.
     *
     * @param object
     *
     * @return Array
     */
    static getKeys(object) {
        // ----------------
        // Check

        if (!TypeManager.isObject(object)) return [];

        // ----------------
        // Process

        return Object.keys(object);
    }

    /**
     * Get object values.
     *
     * @param object
     *
     * @return Array
     */
    static getValues(object) {
        // ----------------
        // Check

        if (!TypeManager.isObject(object)) return [];

        // ----------------
        // Process

        return Object.values(object);
    }
}
