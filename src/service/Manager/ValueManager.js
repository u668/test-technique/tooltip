/**
 * ----------------------------------------------------------------
 * @Author     : Mathieu Boutin
 *
 * @File       : ValueManager.js
 * @Created_at : 26/03/2022
 * @Update_at  : 26/03/2022
 * ----------------------------------------------------------------
 */

import TypeManager from "./TypeManager";

export default class ValueManager {
    // --------------------------------
    // Getter

    /**
     * Get number result for a value.
     * - Number (Depends of variable content)
     * - Default 0
     *
     * @param value
     * @param defaultValue
     *
     * @returns Number
     */
    static getNumber(value, defaultValue = 0) {
        // Check
        if (TypeManager.isNull(value) || TypeManager.isEmpty(value)) return defaultValue;

        // Process
        if (!TypeManager.isNumber(value)) value = Number(value);
        if (!TypeManager.isNumber(value)) return defaultValue;

        return value;
    }
}
