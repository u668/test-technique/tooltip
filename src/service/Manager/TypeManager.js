/**
 * ----------------------------------------------------------------
 * @Author     : Mathieu Boutin
 *
 * @File       : TypeManager.js
 * @Created_at : 26/03/2022
 * @Update_at  : 26/03/2022
 * ----------------------------------------------------------------
 */

export default class TypeManager {
    // --------------------------------
    // Primary

    /**
     * Check if the value is undefined.
     *
     * @param value
     * @returns Boolean
     */
    static isUndefined(value) {
        return typeof value === 'undefined';
    }

    /**
     * Check if the value is null.
     *
     * @param value
     * @returns Boolean
     */
    static isNull(value) {
        return this.isUndefined(value) || value === null;
    }

    /**
     * Check if the value is empty.
     *
     * @param value
     * @returns Boolean
     */
    static isEmpty(value) {
        if (this.isUndefined(value)) return true;
        if (this.isNull(value)) return true;

        if (this.isObject(value)) {
            for (let key in value) {
                if (Object.prototype.hasOwnProperty.call(value, key)) return false;
            }
            return true;
        }
        return (value === '' || value.length <= 0);
    }

    /**
     * Check if the value is a number.
     *
     * @param value
     * @returns Boolean
     */
    static isNumber(value) {
        return typeof value === 'number' && isFinite(value);
    }

    // --------------------------------
    // Complex

    /**
     * Check if the value is an object.
     *
     * @param value
     * @returns Boolean
     */
    static isObject(value) {
        // return value && typeof value === 'object' && value.constructor !== Array;
        return !this.isUndefined(value) && !this.isNull(value) && typeof value === 'object' && value.constructor !== Array;
    }

    /**
     * Check if the value is a function.
     *
     * @param value
     * @returns Boolean
     */
    static isFunction(value) {
        return typeof value === 'function';
    }
}
