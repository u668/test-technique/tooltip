/**
 * ----------------------------------------------------------------
 * @Author     : Mathieu Boutin
 *
 * @File       : TriggerConstant.js
 * @Created_at : 26/03/2022
 * @Update_at  : 26/03/2022
 * ----------------------------------------------------------------
 */

export default class TriggerConstant {
    /**
     * Return hover event.
     *
     * @returns {string}
     */
    static get HOVER(){
        return 'hover';
    }

    /**
     * Return focus event.
     *
     * @returns {string}
     */
    static get FOCUS(){
        return 'focus';
    }
}
