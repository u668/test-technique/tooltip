/**
 * ----------------------------------------------------------------
 * @Author     : Mathieu Boutin
 *
 * @File       : OptionsService.js
 * @Created_at : 26/03/2022
 * @Update_at  : 26/03/2022
 * ----------------------------------------------------------------
 */

import ObjectManager from "../Manager/ObjectManager";
import ValueManager from "../Manager/ValueManager";
import TriggerConstant from "../Constant/TriggerConstant";

export default class OptionsService {
    // --------------------------------
    // Core Methods

    /**
     * Prepare options tooltip.
     *
     * @param options
     * @param defaultOptions
     * @returns {{width: {min: Number, max: Number}, trigger: *}}
     */
    static prepareOptions(options, defaultOptions) {
        // ----------------
        // Vars

        // Options
        let width = ObjectManager.get(options, 'width');
        let trigger = ObjectManager.get(options, 'trigger');

        // Default Options
        let defaultWidth = ObjectManager.get(defaultOptions, 'width');
        let defaultTrigger = ObjectManager.get(defaultOptions, 'trigger');

        // ----------------
        // Prepare

        width = this.prepareWidthOptions(width, defaultWidth);
        trigger = this.prepareTriggerOptions(trigger, defaultTrigger);

        // ----------------
        // Process

        return {
            width: width,

            trigger: trigger
        }
    }


    /**
     * Prepare width options.
     *
     * @param options
     * @param defaultOptions
     * @returns {{min: Number, max: Number}}
     */
    static prepareWidthOptions(options, defaultOptions) {
        // ----------------
        // Vars

        // Options
        let min = ObjectManager.get(options, 'min');
        let max = ObjectManager.get(options, 'max');

        // Default Options
        let defaultMin = ObjectManager.get(defaultOptions, 'min');
        let defaultMax = ObjectManager.get(defaultOptions, 'max');

        // ----------------
        // Prepare

        min = ValueManager.getNumber(min, defaultMin);
        max = ValueManager.getNumber(max, defaultMax);

        // ----------------
        // Process

        return {
            min: min,
            max: max
        }
    }

    /**
     * Prepare trigger options.
     *
     * @param options
     * @param defaultOptions
     * @returns {*}
     */
    static prepareTriggerOptions(options, defaultOptions) {
        // ----------------
        // Prepare

        switch (options) {
            case TriggerConstant.HOVER : {
                break;
            }

            case TriggerConstant.FOCUS : {
                break;
            }

            default:
                options = defaultOptions;
                break;
        }

        // ----------------
        // Process

        return options;
    }
}
