/**
 * ----------------------------------------------------------------
 * @Author      : Mathieu Boutin
 *
 * @File        : routes.js
 * @Created_at  : 26/03/2022
 * @Update_at   : 26/03/2022
 * ----------------------------------------------------------------
 */

// Components
import Test from "../../tests/Test";

export default [
    // --------------------------------
    // Page : Test

    {
        path: '',
        name: 'test',
        component: Test
    }
];