/**
 * ----------------------------------------------------------------
 * @Author      : Mathieu Boutin
 *
 * @File        : Router.js
 * @Created_at  : 26/03/2022
 * @Update_at   : 26/03/2022
 * ----------------------------------------------------------------
 */

// Require needed libraries
import Vue from 'vue';
import VueRouter from 'vue-router';

// Require needed components
import routes from './routes/routes';

export default class Router {
    // --------------------------------
    // Constants

    /**
     * Get Vue Router.
     *
     * @returns {VueRouter}
     */
    static get router() {
        return new VueRouter({
            mode: 'history',
            routes: routes
        });
    }

    // --------------------------------
    // Register

    /**
     * Register the router module in VueJs.
     */
    static register() {
        Vue.use(VueRouter);
    }
}
