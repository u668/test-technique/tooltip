~~# Description Test Technique Unkle.

Ceci est un projet VueJS. Il a pour but de répondre à un besoin technique UI/UX de l'entreprise. Il faut créer un
composant 'tooltip' qui va permettre d'afficher des informations pour certains composants / textes / inputs.

# Installation et Utilisation

Il vous faudra vous munir de l'outil NodeJs qui contient Npm. ([https://kinsta.com/fr/blog/comment-installer-node-js/])

Une fois acquis, il faudra aller dans le projet tooltip.

Pour installer le projet dans un dossier : git clone git@gitlab.com:u668/test-technique/tooltip.git

Ensuite, aller dans le projet puis lancer la commande suivante (au même niveau que le fichier package.json) :

```shell script
$ npm install
```

Cette commande installera le package json dans un dossier nodesModules (commande nécessaire pour installer les
outils/composants techniques nécessaires pour lancer le projet)

Ensuite, pour lancer le projet , utilisez la commande suivante :

```shell script
$ npm run serve
```

Vous verrez deux lignes sur la console, copier en une dans le navigateur (ou CTRL + click dessus).

![img.png](public/Npm/img.png)

# Analyse Technique

Le composant doit pouvoir :

    * Gérer sa position.
    * Gérer son apparition et disparition (cycle de vie).
    * Être trigger par plusieurs event possible (Hover, focus dans un input, etc..)
    * Gérer ses dimensions minimales et maximales.
    * Être animé
    * Peut contenir HTML / Composant / texte / Input

# Problèmes Techniques potentiels

Les plus gros problèmes techniques que l'on pourrait rencontrer sont :

    * Les différents triggers (Hover, focus, ...).
        => Il faut transmettre l'élément HTML sur lequel on veut faire l'effet, et checker si la méthode de l'event existe. 
        Par défault on mettra l'effet Hover (qui existe sur tous les éléments HTML).

    * La position du composant.
        => Comment on choisit la position (en dessous , au dessus, à droite , à gauche du composant ? Ou plutôt au niveau de la souris)
        => Si jamais l'élément HTML est tout en bas de la page , il faut pas que la tooltip soit pas visible (la mettre en dessous n'est donc pas possible dans ce cas).
        => Idem si jamais l'élément HTML est au bord de la page (droite / gauche / en haut / en bas).

# Première Solution

Faire un composant dans lequel on mettra l'élément HTML (slot en
vueJS [https://fr.vuejs.org/v2/guide/components-slots.html]) par un slot. Ainsi dans notre composant on aura l'élément
sur lequel on veut mettre l'effet.(La deuxième solution aurait été de transmettre la reférence vueJS ou l'id de
l'élément et de le récupérer avec du JS mais moins propre je trouve)

Ainsi on aura un composant TooltipContainer pourra être appelé comme ça :

```html

<TooltipContainer>
    <slot name="Element"> Texte / Composant / input / Image</slot>
    <slot name="Tooltip">Texte / composant / input</slot>
</TooltipContainer>

```

Ensuite pour les contraintes techniques :

    * Gérer ses dimensions minimales et maximales
    * Être trigger par plusieurs event possible (Hover, focus dans un input, etc..)

On peut passer au composant une props options dans laquel on pourra ajouter ses options. L'avantage est que si plus tard
on veut ajouter d'autres options, on pourra le faire facilement via cette props. On aura juste à rajouter des clés
supplémentaires à cet objet.

**Autre problème, la compatibilité des options et du élément HTML. Par exemple on ne peut pas focus une div (sauf si on
lui ajoute les bonnes propriété HTML, tabindex)**
Mais ce problème sera laissé de côté (comme dit dans le test technique, on pourra laisser certains problèmes techniques)
.

Par exemple pour la props options :

```json 
    options = {
        // Width gère la taille du tooltip (en px) (Les valeurs sont des nombres)
        "width" : {
            "min" : 10,
            "max" : 200
        },
        
        // Trigger gère le trigger qui va faire apparaître tooltip (valeur par défault si l'évent n'existe pas : 'hover')
        "trigger" : 'hover' ('focus', ...)
    }
```

Pour la contrainte technique :

    * Gérer sa position entièrement 

On peut dans un premier temps sans penser au cas limite (bord de pages), se dire que l'on peut aussi mettre dans options
dans un clé le type de position que l'on veut. La clé origin permet de déterminer par rapport à quel élément on
positionne la tooltip (la souris ou l'élément HTML).

Cette options aura donc comme valeur :

```json 
    options = {
       "position" : {
            "origin" : "mouse" / "element" (Mouse => positionne par rapport à la souris, "element" => posiotionne par rapport à l'élément HTML),
            "top": -10, (nombre, valeur de la position top en px)
            "left" : 0  (nombre, valeur de la position left en px)
       }
    }
```

Pour la contrainte technique :

    * Gérer son apparition / dispariton (cycle de vie)
    * Être animé

On peut utiliser une balise transition ([https://fr.vuejs.org/v2/guide/transitions.html]) et une condition sur le
composant tooltip (v-if); Le booléan sera mis à true lorsque le trigger sera déclenché.

Pour la contrain technique :

    * Peut contenir du HTML / texte / composants / image (en balise html)

On va utiliser la props v-html qui permet d'interpréter le contenu. On passera le contenu par une props content. (On le
met pas dans options car ce n'est pas options)

# Deuxième Solution - Modification de certains détails.

Après avoir relu le test, ma solution sur la position avec la clé top / left ne répond pas à la question, car il faut
donc rentrer la position à laquelle on veut voir le tooltip. Je vais donc enlever la clé position.

La tooltip sera donc maintenant positionné en dessous de la souris si hover (lors de l'entrée en dessus de l'élément, la
tooltip ne suivra pas la souris), et sous l'élément si focus.

J'ai choisi cette solution car on ne peut pas tout le temps mettre la tooltip en dessous de l'élément. Par exemple si
l'élément est un long texte, on ne peut pas mettre la tooltip en dessous de ce texte car l'utilisateur ne sera pas
entrain de regarder à cette endroit.

## Explication Structure

### Service

Dossier contenant des fonctions JS que l'on peut réutiliser partout dans l'application.

Le dossier Manager est un dossier où je stocke mes managers (fonctions qui permettent de manipuler des éléments)
Ce dossier **N'EST PAS MON TRAVAIL** mais celui d'un collègue, que l'on utilise dans notre application à Spopit. Cela
facilie grandement la maniopulation d'objet, d'array. Je n'ai récupéré que les fonctions dont j'avais besoin.

Le dossier Options contient les fonctions JS qui vont permettre de préparer les options du composants tooltip.~~

### Components

Dossier content les différents composants du tooltip + composant test pour mettre dans la tooltip.

Dans la tooltipContainer je prépare les options et je crée les triggers tandis que dans la tooltip je ne fais
qu'afficher et préparer le style css.

# Temps passé : 

Samedi 26/03/2022 : 
    Début : 8h30
    Fin : 13h14