/**
 * ----------------------------------------------------------------
 * @Author      : Mathieu Boutin
 *
 * @File        : vue.config.js
 * @Created_at  : 26/03/2022
 * @Update_at   : 26/03/2022
 * ----------------------------------------------------------------
 */

module.exports = {
    css: {
        extract: false
    }
};
